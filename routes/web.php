<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
   	
}
);
Route::get('/Chapter1',"ChapterController@Chapter1");
Route::get('/Chapter2',"ChapterController@Chapter2");
Route::get('/Chapter3',"ChapterController@Chapter3");
Route::get('/Chapter4',"ChapterController@Chapter4");
Route::get('/Chapter5',"ChapterController@Chapter5");
Route::get('/Chapter6',"ChapterController@Chapter6");
