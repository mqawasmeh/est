<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChapterController extends Controller
{
    public function Chapter1(){
    	return view('Chapter.Chapter1');
    }
    public function Chapter2(){
    	return view('Chapter.Chapter2');
    }
    public function Chapter3(){
    	return view('Chapter.Chapter3');
    }
    public function Chapter4(){
    	return view('Chapter.Chapter4');
    }
    public function Chapter5(){
    	return view('Chapter.Chapter5');
    }
    public function Chapter6(){
    	return view('Chapter.Chapter6');
    }
}
