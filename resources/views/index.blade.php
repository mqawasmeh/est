<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Home</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2>Summarization EST</h2>
            <span class="subheading">English for Sciences & Technology</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
          
          <a href="/Chapter1">
            <h1 class="post-title">
             Chapter 1
            </h1>
          
            <h3 class="post-subtitle">
              Introduction to Scientific Statements
            </h3>
              </a>
         
        </div>
        <hr>
        <div class="post-preview">
           <a href="/Chapter2">
            <h2 class="post-title">
              Chapter 2
            </h2>
            <h3 class="post-subtitle">
              Defining Energy
            </h3>
          </a>
          
        </div>
        <hr>
        <div class="post-preview">
           <a href="/Chapter3">
            <h2 class="post-title">
              Chapter 3
            </h2>
            <h3 class="post-subtitle">
             Comparing the Elements
            </h3>
          </a>
        
        </div>
        <hr>
        <div class="post-preview">
          <a href="/Chapter4">
            <h2 class="post-title">
              Chapter 4
            </h2>
            <h3 class="post-subtitle">
              The word of Business
            </h3>
          </a>
         
        </div>
        <hr>
        <hr>
        <div class="post-preview">
           <a href="/Chapter5">
            <h2 class="post-title">
              Chapter 5
            </h2>
            <h3 class="post-subtitle">
              Classifying The Composition of Matter

            </h3>
          </a>
  
        </div>
        <hr>
        <hr>
        <div class="post-preview">
           <a href="/Chapter6">
            <h2 class="post-title">
              Chapter 6
            </h2>
            <h3 class="post-subtitle">
              Vocabulary of Numbers
            </h3>
          </a>
        </div>
        <hr>
        <hr>
     

  <!-- Footer -->
<footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; <br>Mohammad Qawasmeh<br>Mohammad Imwas<br>Hamza Zhoor<br>Mohammad El Amour</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>

</body>

</html>
