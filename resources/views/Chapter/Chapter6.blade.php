<!DOCTYPE html>
<html>
<head>
	<title>Chapter6</title>
  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
</head>
<body>
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2>Summarization EST</h2>
            <span class="subheading">English for Sciences & Technology</span>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="container">
    <div class="row">
      <div class="col-lg-10  mx-auto">
        <p>
          In this unit the writer explore some words related with numbers in a way fits the scientific writing. Then he moves to talk about some of Geometric Shapes. 
        </p>
        <b> (_) ,  (x OR *) ,  (=) ,   (<) ,    (>)</b>
        <hr>
        <h3>Fractions</h3>
        <table class="table">
          <thead>
            <tr >
              <th scope="col">#</th>
              <th scope="col"><h3>read</h3></th>
              <th scope="col"><h3>write</h3></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td><h3>1/2</h3></td>
              <td><h3>one half</h3></td>
            </tr>
            <tr >
              <th scope="row">2</th>
              <td><h3>1/3</h3></td>
              <td><h3>one thrid</h3></td>
            </tr>
            <tr >
              <th scope="row">3</th>
              <td><h3>1/4</h3></td>
              <td><h3>one quarter</h3></td>
            </tr>
          </tbody>
        </table>
        <hr>
        <h3><b>Percentage</b>  (%)</h3>
        <hr>
        <h3>Numerical Expressions as:</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <h6>
            • Speed <br><br>
            • Weight <br><br>
            • Telephone number <br><br>
            • Decimal <br><br>
            • Date <br><br>
            • Temperature <br><br>
            • Height <br><br>
            • Fraction <br><br>
            • Price <br><br>
            • Score   <br><br>
          </h6>
          <p>
            Be careful when you express large numbers (billion, million, thousand, hundred), they are not followed by an (s). 
            Two hundred NOT two hundreds 
            Three million NOT three millions 
            Five billion NOT five billions 

            Note that, in British English takes ‘and’ between ‘hundred and... .’, but in American English it is omitted. 
            Also, note when you speak about numbers that they are read in the following manner:
            Million, thousand, hundred.
          </p>
        </div>
        <h3>Geometric Shapes</h3>
          <div class="col-lg-12 col-md-10 mx-auto">
            <p>There are several kinds of shapes. In this unit the book explore them with names and drawings. </p>
            <b>Here is a small list about these shapes names: </b>
          <h6>
            <br>
            • Circle <br><br>
            • Triangle <br><br>
            • Square <br><br>
            • Rectangle <br><br>
            • Trapezoid <br><br>
            • Pentagon <br><br>
            • Hexagon <br><br>
            • Heptagon <br><br>
            • Octagon <br><br>
            • Nonagon <br><br>
            • Decagon <br><br>
            • Dodecagon <br><br>
            • Polygons <br><br>
          </h6>
      </div>
    </div>
  </div>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; <br>Mohammad Qawasmeh<br>Mohammad Imwas<br>Hamza Zhoor<br>Mohammad El Amour</p>
        </div>
      </div>
    </div>
  </footer>
</body>
</html>