<!DOCTYPE html>
<html>
<head>
	<title>Chapter2</title>
</head>
<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

<!-- Custom styles for this template -->
<link href="css/clean-blog.min.css" rel="stylesheet">
<body>
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2>Summarization Chapter2</h2>
            <span class="subheading">English for Sciences & Technology</span>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="container">
    <div class="row">
      <div class="col-lg-10  mx-auto">
        <p>
          A definition answers the question “what is it?” It is important especially when a word has more than one meaning. Also, a definition should be complete enough to include all the items that go under the word meaning. When we look up for a word, it might be defined in different ways according to different researchers. The communication between researchers is dependent on precise definitions of substances, concepts, processes, and ideas. 
        </p>
        <hr>
        <h3>Using English to define </h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>
            According to Aristotle, a good definition should include the general classification of a term plus its characteristics. 
          </p>
          <h5>Term = Class + Characteristics </h5>
          <p>Energy is the ability to do work, but when we add another word before it, the definition differs and gives us other types of energy; (chemical energy, mechanical energy, kinetic energy...).</p>
        </div>
        <hr>
        <h3>When defining, remember to:</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <h6>• Definitions require the present simple tense and the verb to be <br><br>
            • The definite article (the) is not used with the term being defined because definitions are general statements 
          </h6>
        </div>
        <br>
        <hr>
        <h3>Sentences Patterns</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <h6>Term = General Class Word + Specific Characteristics  <br><br>
            Term = Specific Characteristics + General Word 
          </h6>
        </div>
        <br>
        <hr>
        <h3>Relative Clauses</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>A relative is a part of a sentence that contains a noun and a verb. Also, it begins with (which, that, where, or who).  In science definitions the most commonly used are (which and that). When referring to people who is used. In this case the relative clauses contain the characteristics that distinguish an item from others in the class. 
            A good point to mention here is that you can simply reverse the definition to see if it is complete. 
          </p>
        </div>
        <hr>
        <h3>How to guess the meaning of the word?</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
         <p>It is by seeing if it is related to one you already know. This also helps you to remember a new word.  You can see if the meaning you have guessed fits the sentence and paragraph.
         The writer in this part presents some prefixes that may help in guessing the meaning of the new word (<b style="color:red";>ecto-, exo-, endo-, micro-, macro-</b>). 
       </p>
       </div>
       <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Prefixes</th>
      <th scope="col">meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>ecto or exto</td>
      <td>outside</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>endo</td>
      <td>inside</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>micro</td>
      <td>small</td>
    </tr>
    <tr>
      <th scope="row">4</th>
      <td>macro</td>
      <td>large</td>
    </tr>
  </tbody>
</table>
<hr>
       <h3>Note-Taking</h3>
       <br>
       <div class="col-lg-12 col-md-10 mx-auto">
       <h6>
        •  Speed is important<br><br>
        • Don’t write in perfect sentences or correct grammar<br><br>
        • Clear enough to make sense<br><br>
        • Avoid using single words or phrases<br><br>
        • List items as subtopics of a main idea

       </h6>

       </div>
       <br>
       <hr>
       <h3>Extended Definitions </h3>
  
       <div class="col-lg-12 col-md-10 mx-auto">
       <p>
         An extended definition includes the basic parts of a formal definition (Class + Characteristics) besides additional information (description, examples, classification, comparison, explanation, or other details). It is used when a concept is too complex to be defined in one or two sentences.
       </p>
       </div>
       <hr>
       <h3>Finding Main Ideas</h3>
       <br>
       <div class="col-lg-12 col-md-10 mx-auto">
       <h6>
        •  It is an important skill to know what an article is about.<br><br> 
        • Each paragraph has a main idea.<br><br> 
        • Sometimes the main idea is expressed in one sentence

       </h6>
       </div>
       <br>
       <hr>
       <h3>Finding Main Ideas</h3>
       <div class="col-lg-12 col-md-10 mx-auto">
       <p>
       _As you read, keep asking yourself about the topic and what claims the writer makes about it <br><br>
       _Pay special attention to the first sentence<br><br>
      _Pay attention to the last sentence of the paragraph
       </p>
       </div>
     </div>
   </div>
 </div>
 <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; <br>Mohammad Qawasmeh<br>Mohammad Imwas<br>Hamza Zhoor<br>Mohammad El Amour</p>
        </div>
      </div>
    </div>
  </footer>

 <script src="vendor/jquery/jquery.min.js"></script>
 <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

 <!-- Custom scripts for this template -->
 <script src="js/clean-blog.min.js"></script>
</body>
</html>