<!DOCTYPE html>
<html>
<head>
	<title>Chapter3</title>
  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
</head>
<body>
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2>Summarization Chapter3</h2>
            <span class="subheading">English for Sciences & Technology</span>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="container">
    <div class="row">
      <div class="col-lg-10  mx-auto">
        <p>
          Scientists tend to organize information by relationships. Comparing is also a way of arranging information and expanding it. In addition, comparing provide a new perspective on information. Comparisons are a part of every aspect of science. 
        </p>
        <hr>
        <h3>When defining, remember to:</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>Comparing is examining two or more items to discover their similarities and differences
            Comparing depends on using different patterns:
          </p>
          <h6>
            • Comparing similarities (like, similar to, comparable to, as important as, resembles, parallels)<br><br>
            •  Contrasting differences (is unlike, is different from, differs from, relatively, comparatively) 
          </h6>
          <p>Tables, charts, and graphs organize information to enable us to see comparisons readily.</p>
        </div>
        <hr>
        <h3>Like and As </h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>
            Comparisons frequently use the words like and as. <br>
            _like is used before a noun or pronoun <br>
            _As is used if the noun is followed by a verb
            <br>
            _As is used before and after and adjective
          </p>
        </div>
        <hr>
        <h3>Skimming</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>
          It is a strategy where you read the passage quickly without reading it all to get a general idea of its items.</p>
          <h6>
            • Read the title and any subtitles <br><br>
            • Read the introduction <br><br>
            • Read the first sentence of every paragraph <br><br>
            • Read the summary or conclusion at the end of the passage

          </h6>
        </div>
        <br>
        <hr>
        <h3>Note-Taking</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>
          The purpose of this skill is to record the main idea in order to study or review them later.  For those who study a foreign language, it is useful to preread.  It introduces you to the new terms and helps you to recognize the basic concepts of the lesson. </p>
        </div>
        <hr>
        <h3>Discussing points</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>
            When you compare items:
            <div class="col-lg-12 col-md-10 mx-auto">
              <p>
                _Begins with similarities <br> 
                _Point out the differences <br>
                _Avoid vague statements

              </p>
            </div> </p>
          </div>
          <hr>
          <h3>Finding Supporting Details</h3>
          <div class="col-lg-12 col-md-10 mx-auto">
            <p>
            Each paragraph has a main idea that contains (a topic and a claim). The main idea is followed with specific information which called supporting details. These are (examples, numbers, facts, and reasons). These details will help you understand what you read. </p>
            <b>There are a large number of transition words to signal a supporting detail:</b>
            <div class="col-lg-12 col-md-10 mx-auto">
              <h6>
                <br>
                • Signal supporting details (also, in addition, furthermore, to begin with, …)<br><br>
                • Signal examples (for example, for instance, including, such as, …)<br><br>
                • Signal a reason or explanation (because, so, as a result, since, therefore)<br><br>

              </h6>
            </div>
          </div>
          <hr>
          <h3>Strategies</h3>
          <div class="col-lg-12 col-md-10 mx-auto">
            <p>
              _Identify the main idea <br>
              _look for examples, facts, numbers and reasons and number them <br>
              _write an outline paragraph 
            </p>
          </div>
        </div>
      </div>
    </div>
    <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; <br>Mohammad Qawasmeh<br>Mohammad Imwas<br>Hamza Zhoor<br>Mohammad El Amour</p>
        </div>
      </div>
    </div>
  </footer>

  </body>
  </html>