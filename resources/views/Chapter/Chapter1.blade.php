<!DOCTYPE html>
<html>
<head>
	<title>Chapter1</title>
  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
</head>
<body>
	<header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2>Summarization Chapter1</h2>
            <span class="subheading">English for Sciences & Technology</span>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="container">
    <div class="row">
      <div class="col-lg-10  mx-auto">
       <p>
        Be and have in scientific statements
        In this unit the writer gives a general idea about scientific statements and how to present them in a correct grammatical way.  Also, he explores some strategies to understand vocabulary in context. 
        Scientific textbooks are books that contain information about the present state of scientific knowledge and describe experiments that serve people, so that the main verb of sentences should be in the present simple tense. Here be careful that the subject agrees with the verb. 
        In spoken English the noun parts are often simple while the verb parts are complicated unlike in the written scientific English. This leads to a difficulty for those who speak languages because there is no need to use (<b style="color:red";>be</b> and<b style="color:red";> have</b>) always. On the other hand, in written sentences there must be at least one main verb. 
      </p>
      <hr>
      <h3>When do you use the present simple?</h3>
      <div class="col-lg-12 col-md-10 mx-auto">
       <h6>
         •	regular actions and regular processes
         <br><br>
         •	general statements 
         <br><br>
         •	factual statements and observations 
         <br><br>
         •	descriptions of experiments 
       </h6>
     </div>
     <p>Generally, you should use present simple unless there are good reasons for using another tense.  </p>
      <hr>
     <h3>How to form the present simple? </h3>
     <div class="col-lg-12 col-md-10 mx-auto">
       <p>
        When the subject is in the third person singular (he, she, it), a large S is been used as a reminder. Also, when it is not a simple matter of adding s to the base form of the verb you should use a large S. 
      </p>
    </div>
    <h6>• Verbs ending in (<b style="color:red";>ss, sh, ch, x, o</b>) we add (<b style="color:red";>es</b>) to the base. </h6>
    <br>
    <h6>• Verbs ending in (<b style="color:red";>y</b>) after a consonant change (<b style="color:red";>y</b>) to (<b style="color:red";>i</b>) and add (<b style="color:red";>es</b>).</h6>
    <br>
    <h6>• Verbs ending in (<b style="color:red";>y</b>) after a vowel follows the main rule (add <b style="color:red";>s</b>). </h6><br>
    <hr>
    <h3>Example</h3>
    <table class="table table-dark">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Verb</th>
          <th scope="col">Present Simple</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Wiegh</td>
          <td>Wieghs</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Pass</td>
          <td>Passes</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>Study</td>
          <td>Studies</td>
        </tr>
      </tbody>
    </table>
    <br>
    <hr>
    <h3>What about negative statements and questions? </h3>
    <div class="col-lg-12 col-md-10 mx-auto">
     <h6>
      • They are much less common in written scientific English.
      <br><br> 
      • They both formed with the help of the verb do, which is ‘empty’ of meaning.

    </h6>
  </div>
  <br>
  <hr>
  <h3>How to understand vocabulary in context? </h3>
  <div class="col-lg-12 col-md-10 mx-auto">
   <p>
    To figure out the meanings of new and unfamiliar words, the reader should look for context clues (words, part of words, and sentences that are near or around the unfamiliar words). This skill saves time instead of looking it up from dictionary. 
  </p>
</div>
<br>
<hr>
<h3>The language of vocabulary in context </h3>
<div class="col-lg-12 col-md-10 mx-auto">
	<p>The writer explores some of clue signals (words or phrases that tell the reader that a context clue is coming). So, this helps the reader to figure out meanings. </p>
	<h6>
   •	Definition (X,or , X is , X means , X is defined as , X is also called , X is also known as)<br><br>
   •	Exemplification (for example , for instance , such as)
   <br> <br>
   •	Contrast (in contrast , but , however , on the other hand , unlike , whereas) 

 </h6>
</div>
<br>
<hr>
<h3>Strategies</h3>
<div class="col-lg-12 col-md-10 mx-auto">
  <p>
   _When you read an unfamiliar word, don’t stop; continue to the end. <br><br>
   _If you don’t understand the general meaning, return to the unfamiliar word.<br><br>
   _you should search for signal words and phrases that introduce context clues.

 </p>
</div>
</div>
</div>
</div>
<footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p class="copyright text-muted">Copyright &copy; <br>Mohammad Qawasmeh<br>Mohammad Imwas<br>Hamza Zhoor<br>Mohammad El Amour</p>
        </div>
      </div>
    </div>
  </footer>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Custom scripts for this template -->
<script src="js/clean-blog.min.js"></script>
</body>
</html>