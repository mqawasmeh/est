<!DOCTYPE html>
<html>
<head>
	<title>Chapter5</title>
  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
</head>
<body>
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2>Summarization Chapter5</h2>
            <span class="subheading">English for Sciences & Technology</span>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="container">
    <div class="row">
      <div class="col-lg-10  mx-auto">
        <p>
          The word classification comes from the word class which means a group of things that all have one important element in common. Scientists group related information into an array. It is an important operation that leads scientists to predict items characteristics.  It is very basic to scientific thought and expression. 
        </p>
        <hr>
        <h3>Using English to Classify</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <b>A classification includes:</b>
          <div class="col-lg-12 col-md-10 mx-auto">
            <h6>
              <br>
              • A general class <br><br>
              • A specific item or items <br><br>
              • A basis for classification <br><br>
            </h6>
          </div>
          <p>The general class is matter. The specific items are solid, liquid, and gas. The basis for classification is the physical state of matter, etc. </p>
        </div>
        <hr>
        <h3>Notes</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>
            _Classification is like an upside-down tree. <br>
            _The passive form is used frequently in sentences of classification <br>
            _The present simple tense is the most commonly used in scientific writing <br>
            _In a sentence like, oxygen is a gas; only the meaning of the words will reveal which is the general category and which is the specific item
          </p>
        </div>
        <hr>
        <h3>Impersonal Scientific Statement- the passive</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>The passive is used frequently in classifying and in all scientific writing. This is because the emphasis in science is usually on the action, not on the person. In active form, the subject performs the action. In passive, the subject receives the action. </p>
          <p>If the action is in the present, use the present tense of the verb to be. Use is, when the subject is singular. Use are, when it is plural. But when the action is in the past, use the past form of the verb be: was, were.
          </p>
          <b>How to form the passive?</b>
          <br>
          <br>
          
          <b>All finite passives are formed by some part of the verb ‘be’ plus the past participle.</b>
          <div class="col-lg-12 col-md-10 mx-auto">
            <h5 style="color:red";><br>Present passive   =    is/are   +    past participle</h5>
            <br>
          </div>
          <hr>
          <b>What about the position of the adverbs?</b>
          <div class="col-lg-12 col-md-10 mx-auto">
            <br>
            <h5 style="color:red";>The modal passive    =    modals + be + past participle</h5>
            <br>
          </div>
          <hr>
          <b>How to make negative statements and questions in the passive? </b>
          <div class="col-lg-12 col-md-10 mx-auto">
            <p>
              _Modals and be are most commonly used in both questions and negative. <br> 
              _Not is usually comes after modal verbs or verb to be in negatives. 
            </p>
          </div>
        </div>
        <hr>
        <h3>Comparing</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <b>In scientific writing, there are two basic methods of comparing items:</b>
          <div class="col-lg-12 col-md-10 mx-auto">
            <br>
            <h6>
              • Describe the characteristics of the first item and then the characteristics of the second. <br><br>
              • Compare both items one characteristics at a time. <br><br>

            </h6>
          </div>
        </div>
        <hr>
        <h3>Vocabulary Building</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <h6>
            <br>
            • Help you with every aspect of communication <br><br>
            • Improve reading and listening comprehension <br><br>
            • Enable the reader to speak and write better <br><br>
            • Help you succeed in any profession <br><br>
          </h6>
        </div>
        <hr>
        <h3>Finding Main Idea </h3>
        <div class="col-lg-12 col-md-10 mx-auto">
          <p>
           Scientific writing consists of concepts which are usually stated in a topic sentence, while the topic sentence is the part that covers all the information in the paragraph. Then you find the supporting material that is information that explains, clarifies, or proves what is stated in the topic. The topic sentence may come in the first sentence, the second, or the last one. It unifies the paragraph and makes it understandable to the reader. 
         </p>
       </div>
       <hr>
       <h3>Note-Taking</h3>
       <div class="col-lg-12 col-md-10 mx-auto">
        <p>
          What you record of notes, you will retain. This skill will not interfere with your listening. It will help you to focus on what is being said.  
        </p>
      </div>

    </div>
  </div>
</div>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p class="copyright text-muted">Copyright &copy; <br>Mohammad Qawasmeh<br>Mohammad Imwas<br>Hamza Zhoor<br>Mohammad El Amour</p>
      </div>
    </div>
  </div>
</footer>
</body>
</html>