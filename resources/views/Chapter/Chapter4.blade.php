<!DOCTYPE html>
<html>
<head>
	<title>Chapter4</title>
  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
</head>
<body>
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2>Summarization Chapter4</h2>
            <span class="subheading">English for Sciences & Technology</span>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="container">
    <div class="row">
      <div class="col-lg-10  mx-auto">
        <p>
          Academic writing often includes information about numbers. Writers use a range of vocabulary to talk about numbers. 
        </p>
        <hr>
        <h3>The language of numbers</h3>
        <div class="col-lg-12 col-md-10 mx-auto">
         <h6>
          • Growing numbers (to increase, to grow, to rise, to go up, …) <br><br>
          • Falling numbers (to decrease, to fall, to decline, to drop, to go down)<br><br>
          • Describe change (rapid, significant, dramatic, steady, sharp, …)<br><br>
        </h6>
      </div>
      <hr>
      <h3>Strategies</h3>
      <div class="col-lg-12 col-md-10 mx-auto">
       <p>
        _Use any numbers in a reading to help you understand the vocabulary <br>
        _learn vocabulary in groups of synonyms <br>
        _make sure you figure out the part of speech
      </p>
    </div>
    <hr>
    <h3>Supply and demand in the global economy </h3>
    <div class="col-lg-12 col-md-10 mx-auto">
     <p>
      _Think about the topic and what you already know about it
      <br>
      _Graphs can help you get an idea about an article before you start reading
    </p>
  </div>
  <hr>
  <h3>Information in graphs and charts </h3>
  <div class="col-lg-12 col-md-10 mx-auto">
   <p>
    Information is easier to understand when it is presented this way. There are different forms of graphic information. The most common ones are (par graphs, line graphs, tables, and pie charts). <br>Good readers try to see the connection between the information in the graphic material and the text.
  </p>
</div>
<hr>
<h3>Strategies</h3>
<div class="col-lg-12 col-md-10 mx-auto">
 <h6>
  <br>
  •  look at the graphic material before you begin reading <br><br>
  • look at the title and other words <br><br>
  • look at the axes <br><br>
  • read the text quickly to see if there is a reference to the graphic material <br><br>
  • when you take notes on a text, don’t forget to take notes on the graphic <br><br>
  • write down one or two sentences about what the graphic information shows <br><br>
</h6>
</div>
<hr>
<h3>Making Connections</h3>
<div class="col-lg-12 col-md-10 mx-auto">
 <p>
  Coherence is something that can be achieved by clearly ways writers connect sentences. In other words they create it by repeating key words. For example, coherence can be created by using pronouns or transition words
</p>
</div>
<hr>
<h3>Scanning for specific information </h3>
<div class="col-lg-12 col-md-10 mx-auto">
 <p>
   When you need to find a specific piece of information from a reading (a number, a name, a definition, or an example), it is not necessary to read the complete text. Instead you can use scanning which is moving your eyes very quickly through a reading until you find the information you are looking for. 
 </p>
</div>
<hr>
<h3>Strategies </h3>
<div class="col-lg-12 col-md-10 mx-auto">
 <h6>
  <br>
  • Decide what specific information you are looking for <br><br> 
  • Focus on a key word <br><br> 
  • Don’t say the word to yourself <br><br> 
  • Scan quickly until you find the key word <br><br>
  • Slow down and see if the information you are looking for is there <br><br>
  • If it is not speed up again and look for the next time <br>
  
</h6>
</div>
</div>
</div>
</div>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <p class="copyright text-muted">Copyright &copy; <br>Mohammad Qawasmeh<br>Mohammad Imwas<br>Hamza Zhoor<br>Mohammad El Amour</p>
      </div>
    </div>
  </div>
</footer>

</body>
</html>